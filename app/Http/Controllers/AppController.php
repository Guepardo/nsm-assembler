<?php

namespace App\Http\Controllers;

use App\App;
use App\Http\Requests\FormApp;
use App\Services\App\CreateAppService;

class AppController extends Controller
{

    public function create()
    {
        return view('apps.create');
    }

    public function store(FormApp $request)
    {
        $service = new CreateAppService($request->all());
        $service->execute();
        return redirect()->route('home');
    }
}
