<?php

namespace App\Services\App;

use App\App;
use App\Services\Builds\CreateBuildService;

class CreateAppService
{
    public function __construct(array $args)
    {
        $this->args = $args;
    }

    public function execute()
    {
        $app = new App($this->args);

        if ($app->save()) {
            return $this->createNewBuild($app);
        }

        return false;
    }

    private function createNewBuild(App $app)
    {
        $service = new CreateBuildService($app, $this->args);
        return $service->execute();
    }
}
