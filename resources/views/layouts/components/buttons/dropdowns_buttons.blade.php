<div class="btn-group">
    <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      {{ $title }}
    </button>
    <div class="dropdown-menu">
        {{ $slot }}
    </div>
  </div>