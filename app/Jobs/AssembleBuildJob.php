<?php

namespace App\Jobs;

use App\Build;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Services\Assemblers\BuildAssemblerService;

class AssembleBuildJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $build;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Build $build)
    {
        $this->build = $build;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $service = new BuildAssemblerService($this->build);
        $service->execute();

        $this->build->processed = true;
        $this->build->save();
    }
}
