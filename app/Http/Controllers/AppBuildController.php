<?php

namespace App\Http\Controllers;

use App\App;
use Illuminate\Http\Request;
use App\Services\Builds\CreateBuildService;

class AppBuildController extends Controller
{
    public function index(App $app)
    {
        $builds = $app->builds()
            ->orderBy('id', 'DESC')
            ->paginate(5);

        return view('app_build.index', compact('builds', 'app'));
    }

    public function create(App $app)
    {
        $build = $app->builds()->latest()->first();
        return view('app_build.create', compact('build'));
    }

    public function store(Request $request, App $app)
    {
        $config = $request->all();

        (new CreateBuildService($app, $config))->execute();

        return redirect()->route('builds.logs.index', $app->latestBuild());
    }
}
