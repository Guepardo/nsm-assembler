<?php

namespace App\Http\Controllers;

use App\Build;

class BuildLogController extends Controller
{
    public function index(Build $build)
    {
        $logs = $build->logs;
        return view('build_log.index', compact('logs', 'build'));
    }
}
