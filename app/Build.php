<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Build extends Model
{
    protected $fillable = [
        'config',
    ];

    protected $casts = [
        'config' => 'json',
    ];

    public function app()
    {
        return $this->belongsTo(\App\App::class);
    }

    public function logs()
    {
        return $this->hasMany(\App\Log::class);
    }
}
