<?php

namespace App\Services\Shell;

use Symfony\Component\Process\Process;

class BaseShell
{
    const PROCESS_TIMEOUT = 3600;

    protected $logger;

    public function __construct($logger = null)
    {
        $this->logger = $logger;
    }

    private function createProcess($command)
    {
        $process = Process::fromShellCommandline($command);

        $process->setWorkingDirectory($this->getWorkDirectory());
        $process->setTimeout(self::PROCESS_TIMEOUT);

        return $process;
    }

    public function shell(array $command)
    {
        $command = implode(' ', $command);
        $process = $this->createProcess($command);
        $process->run();

        if (!$process->isSuccessful()) {
            $output = $process->getErrorOutput();
            $this->logger->critical($command, $output);
            return false;
        }

        $output = $process->getOutput();
        $this->logger->notice($command, $output);

        return $output;
    }

    public function asyncShell(array $command)
    {
        $command = implode(' ', $command);

        $this->logger->notice('Running Async: ' . $command, '');

        $process = $this->createProcess($command);
        $process->start();

        return $process;
    }

    protected function getWorkDirectory()
    {
        return env('TEMPLATE_FULL_PATH');
    }
}
