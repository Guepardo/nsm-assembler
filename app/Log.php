<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    protected $fillable = [
        'type',
        'command',
        'result',
    ];

    public function build()
    {
        return $this->belongsTo(\App\Build::class);
    }
}
