@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                  <b> Build {{$build->id}}</b> logs
                </div>

                <div class="card-body">
                    @component('layouts.components.box', ['title' => 'Logs'])
                      <table class="table table-striped">
                        <thead>
                          <tr>
                            <th scope="col">#</th>
                            <th scope="col">type</th>
                            <th scope="col">command</th>
                            <th scope="col">result</th>
                          </tr>
                        </thead>
                        <tbody>
                          @foreach ($logs as $log)
                            <tr>
                                <th scope="row">{{ $log->id }}</th>
                                <td>{{ $log->type }}</td>
                                <td>{{ $log->command }}</td>
                                <td>{{ $log->result }}</td>
                            </tr>
                          @endforeach
                        </tbody>
                      </table>
                    @endcomponent
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
