@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Create App</div>
                <div class="card-body">
                  @include('layouts.partials.errors')

                  <form method="post" action="{{ route('apps.store') }}" enctype="multipart/form-data" >
                      @csrf
                      @include('apps.partials.form')
                  </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
