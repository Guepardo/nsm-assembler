<form>
  @component('layouts.components.box', ['title' => 'Informações Básica'])
  <div class="row">
    <div class="col-md-6">
        <label for="inputZip">Nome do App</label>
        <input type="text" class="form-control" disabled name="app_name" value="{{ old('app_name') ?? $build->app->app_name }}">
    </div>
    <div class="col-md-6">
        <label for="inputZip">Nome do App no telefone</label>
        <input type="text" class="form-control" disabled name="app_display_name" value="{{ old('app_display_name') ?? $build->app->app_display_name }}" >
    </div>
  </div>
  @endcomponent

  @component('layouts.components.box', ['title' => 'Definições de Cores'])
    <div class="row">
      <div class="col-md-6">
          <label for="inputZip">tab_icon_default</label>
          <input type="color" class="form-control" value="{{data_get($build->config, 'tab_icon_default')}}" name="config[tab_icon_default]" >
      </div>
    </div>
    <div class="row">
      <div class="col-md-6">
          <label for="inputZip">tint_color</label>
          <input type="color" class="form-control" value="{{data_get($build->config, 'tint_color')}}" name="config[tint_color]">
      </div>
    </div>
    <div class="row">
      <div class="col-md-6">
          <label for="inputZip">tab_bar</label>
          <input type="color" class="form-control" value="{{data_get($build->config, 'tab_bar')}}" name="config[tab_bar]">
      </div>
    </div>
    <div class="row">
      <div class="col-md-6">
          <label for="inputZip">error_background</label>
          <input type="color" class="form-control" value="{{data_get($build->config, 'error_background')}}" name="config[error_background]">
      </div>
    </div>
    <div class="row">
      <div class="col-md-6">
          <label for="inputZip">error_text</label>
          <input type="color" class="form-control" value="{{data_get($build->config, 'error_text')}}" name="config[error_text]">
      </div>
    </div>
    <div class="row">
      <div class="col-md-6">
          <label for="inputZip">warning_background</label>
          <input type="color" class="form-control" value="{{data_get($build->config, 'warning_background')}}" name="config[warning_background]">
      </div>
    </div>
    <div class="row">
      <div class="col-md-6">
          <label for="inputZip">warning_text</label>
          <input type="color" class="form-control" value="{{data_get($build->config, 'warning_text')}}" name="config[warning_text]">
      </div>
    </div>
    <div class="row">
      <div class="col-md-6">
          <label for="inputZip">notice_text</label>
          <input type="color" class="form-control" value="{{data_get($build->config, 'notice_text')}}" name="config[notice_text]">
      </div>
    </div>
  @endcomponent

  @component('layouts.components.box', ['title' => 'Imagens'])
    <div class="row">
      <div class="col-md-6">
          <label for="inputZip">Ícone do App (512x512) </label>
          <input type="file"  value="{{data_get($build->config, 'app_icon')}}" name="config[app_icon]">
      </div>

      <div class="col-md-6">
          <label for="inputZip">Splash Screen (2208x2208)</label>
          <input type="file"  value="{{data_get($build->config, 'app_splash_screen')}}" name="config[app_splash_screen]" >
      </div>
    </div>
  @endcomponent

  @component('layouts.components.box', ['title' => 'Publicação'])
    @component('layouts.components.box', ['title' => 'Credencias do Expo'])
    <div class="row">
        <div class="col-md-6">
            <label for="inputZip">Usuário</label>
            <input type="text" class="form-control" value="{{data_get($build->config, 'expo_user')}}" name="config[expo_user]">
        </div>

        <div class="col-md-6">
            <label for="inputZip">Senha</label>
            <input type="password" class="form-control" value="{{data_get($build->config, 'expo_password')}}" name="config[expo_password]">
        </div>
      </div>
    @endcomponent
  @endcomponent

  <button type="submit" class="btn btn-primary">Create</button>
</form>