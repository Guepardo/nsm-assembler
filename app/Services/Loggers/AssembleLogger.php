<?php

namespace App\Services\Loggers;

use App\Build;
use Illuminate\Support\Facades\Log;

class AssembleLogger
{
    public function __construct(Build $build)
    {
        $this->build = $build;
    }

    public function critical($command, $output)
    {
        [$command, $output] = $this->sanitize($command, $output);

        Log::critical($command . ' ' . $output);

        $this->build->logs()->create([
            'type' => 'CRITICAL',
            'command' => $command,
            'result' => $output,
        ]);
    }

    public function notice($command, $output)
    {
        [$command, $output] = $this->sanitize($command, $output);

        Log::notice($command . ' ' . $output);

        $this->build->logs()->create([
            'type' => 'NOTICE',
            'command' => $command,
            'result' => $output,
        ]);
    }

    private function sanitize($command, $output)
    {
        if (str_contains(strtolower($command), 'password') !== false) {
            $comMand = '**HIDDEN**';
            $output = '**HIDDEN**';
        }

        return [$command, $output];
    }
}
