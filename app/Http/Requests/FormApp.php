<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FormApp extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'app_name' => 'required',
            'app_display_name' => 'required',
            "config.tab_icon_default" => 'required',
            "config.tint_color" => 'required',
            "config.tab_bar" => 'required',
            "config.error_background" => 'required',
            "config.error_text" => 'required',
            "config.warning_background" => 'required',
            "config.warning_text" => 'required',
            "config.notice_text" => 'required',
            "config.app_icon" => 'required',
            "config.app_splash_screen" => 'required',
            "config.expo_user" => 'required',
            "config.expo_password" => 'required',
        ];
    }
}
