<?php

namespace App\Services\Shell;

use App\Services\Shell\BaseShell;

class ExpoShellService extends BaseShell
{
    public function login($username, $password)
    {
        return $this->shell([
            'expo',
            'login',
            '--username',
            $username,
            '--password',
            $password,
        ]);
    }

    public function logout()
    {
        return $this->shell([
            'expo',
            'logout',
        ]);
    }

    public function publish()
    {
        return $this->shell([
            'expo',
            'publish',
        ]);
    }

    public function bundle()
    {
        return $this->asyncShell([
            'BROWSER=none',
            'expo start',
        ]);
    }

    public function killBundle($bundleProcess)
    {
        $bundleProcess->signal('9');
    }

    public function optimize()
    {
        return $this->shell([
            'expo',
            'optimize',
        ]);
    }
}
