<?php

namespace App\Services\Shell;

use App\Services\Shell\BaseShell;

class CommandShellService extends BaseShell
{
    public function cp($source, $destination)
    {
        return $this->shell([
            'cp',
            $source,
            $destination,
        ]);
    }

    public function findByName($name)
    {
        return $this->shell([
            'find',
            '-name',
            $name,
        ]);
    }

    public function sed($filePath, $target, $replace)
    {
        return $this->shell([
            'sed',
            '-ie',
            "s/{$target}/{$replace}/g {$filePath}",
        ]);
    }
}
