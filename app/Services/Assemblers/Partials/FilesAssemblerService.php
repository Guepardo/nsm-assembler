<?php

namespace App\Services\Assemblers\Partials;

use App\Services\Assemblers\BaseAssembler;

class FilesAssemblerService extends BaseAssembler
{
    const TEMPLATE_NAME = '*.tpl.*';

    public function execute()
    {
        $templates = $this->getAbsoluteTemplatesPath();
        $this->replaceMarkupToValues($templates);
    }

    private function getAbsoluteTemplatesPath()
    {
        $templatesAvailable = $this->commands
            ->findByName(self::TEMPLATE_NAME);

        return explode("\n", $templatesAvailable);
    }

    private function replaceMarkupToValues(array $templatesAbsolutePath)
    {
        $buildConfigs = $this->build->config;

        foreach ($templatesAbsolutePath as $templatePath) {
            $filePath = $this->copyTemplateFileToProductionMode($templatePath);

            foreach ($buildConfigs as $key => $value) {
                $this->commands->sed($filePath, "%{$key}%", $value);
            }
        }
    }

    private function copyTemplateFileToProductionMode($templatePath)
    {
        $fileName = basename($templatePath);
        $dirname = dirname($templatePath);

        $newFileName = str_replace('.tpl.', '.', $fileName);

        $productionFilePath = "{$dirname}/$newFileName";

        $this->commands->cp($templatePath, $productionFilePath);

        return $productionFilePath;
    }
}
