@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                  <b> {{$app->app_name}}</b> Builds
                </div>

                <div class="card-body">
                    @component('layouts.components.box', ['title' => 'Builds'])
                      <table class="table table-striped">
                        <thead>
                          <tr>
                            <th scope="col">#</th>
                            <th scope="col">Created at</th>
                            <th scope="col">Running?</th>
                            <th scope="col">Actions</th>
                          </tr>
                        </thead>
                        <tbody>
                          @foreach ($builds as $build)
                            <tr>
                                <th scope="row">{{ $build->id }}</th>
                                <td>{{ $build->created_at->diffForHumans() }}</td>
                                <td>{{ $build->processed }}</td>
                                <td>
                                  <a class="btn btn-link" href="{{ route('builds.logs.index', $build)}}">Assemble Logs</a>
                                </td>
                            </tr>
                          @endforeach
                        </tbody>
                      </table>
                      {{ $builds->links() }}
                    @endcomponent
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
