<?php

namespace App\Services\Assemblers;

use App\Services\Assemblers\BaseAssembler;
use App\Services\Assemblers\Partials\FilesAssemblerService;
use App\Services\Assemblers\Partials\ImagesAssemblerService;

class BuildAssemblerService extends BaseAssembler
{
    public function execute()
    {
        $this->assemble();
        $this->publish();
    }

    public function assemble()
    {
        $this->git->checkout('master');
        $this->git->resetOrigin('master');

        $branchName = snake_case($this->build->app->app_name);

        if (!$this->git->newBranch($branchName)) {
            $this->git->checkout($branchName);
        }

        (new ImagesAssemblerService($this->build))->execute();
        (new FilesAssemblerService($this->build))->execute();

        $this->git->addFiles();
        $this->git->commit();
    }

    public function publish()
    {
        $username = $this->build->config['expo_user'];
        $password = $this->build->config['expo_password'];

        $bundleProcess = $this->expo->bundle();
        sleep(10);
        $this->expo->logout();
        $this->expo->login($username, $password);
        $this->expo->publish();
        $this->expo->killBundle($bundleProcess);
    }
}
