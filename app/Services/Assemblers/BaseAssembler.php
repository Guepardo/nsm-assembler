<?php

namespace App\Services\Assemblers;

use App\Build;
use App\Services\Shell\GitShellService;
use App\Services\Loggers\AssembleLogger;
use App\Services\Shell\ExpoShellService;
use App\Services\Shell\CommandShellService;

class BaseAssembler
{
    protected $build;

    protected $git;

    protected $expo;

    protected $commands;

    public function __construct(Build $build)
    {
        $this->build = $build;

        $logger = new AssembleLogger($build);

        $this->git = new GitShellService($logger);
        $this->expo = new ExpoShellService($logger);
        $this->commands = new CommandShellService($logger);
    }
}
