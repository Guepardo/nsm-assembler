@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Dashboard
                <a href="{{ route('apps.create')}}" class="btn btn-primary float-right">New App</a>
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    @component('layouts.components.box', ['title' => 'Apps'])
                      <table class="table table-striped">
                        <thead>
                          <tr>
                            <th scope="col">#</th>
                            <th scope="col">Icon</th>
                            <th scope="col">Name</th>
                            <th scope="col">Num. Builds</th>
                            <th scope="col">...</th>
                          </tr>
                        </thead>
                        <tbody>
                          @foreach ($apps as $app)
                            <tr>
                                <th scope="row">{{ $app->id }}</th>
                                <td>
                                  <img src="{{ asset($app->latestBuild()->config['app_icon']) }}" alt="">
                                </td>
                                <td>{{ $app->app_name }}</td>
                                <td>{{ $app->unProcessedBuilds->count() }}</td>
                                <td>
                                    @component('layouts.components.buttons.dropdowns_buttons', [ 'title' => '...'])
                                      <a class="dropdown-item" href="{{ route('apps.builds.create', $app) }}">New Build</a>
                                      <a class="dropdown-item" href="{{ route('apps.builds.index', $app) }}">See All Builds</a>
                                    @endcomponent
                                </td>
                            </tr>
                          @endforeach
                        </tbody>
                      </table>
                      {{ $apps->links() }}
                    @endcomponent
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
