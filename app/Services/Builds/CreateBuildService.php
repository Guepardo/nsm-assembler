<?php

namespace App\Services\Builds;

use App\App;
use App\Jobs\AssembleBuildJob;

class CreateBuildService
{
    public function __construct(App $app, array $args)
    {
        $this->app = $app;
        $this->args = $args;
    }

    public function execute()
    {
        $imagesSaved = $this->saveImages();

        if (!$imagesSaved) {
            return false;
        }

        $buildConfigs = $this->getBuildConfigs();

        $build = $this->app->builds()
            ->create($buildConfigs);

        if ($build) {
            AssembleBuildJob::dispatch($build)
                ->delay(now()->addSeconds(5));
        } else {
            return false;
        }

        return true;
    }

    private function getBuildConfigs()
    {
        return [
            'config' => $this->args['config'],
        ];
    }

    private function saveImages()
    {
        if (!array_key_exists('config', $this->args)) {
            return false;
        }

        $this->saveImage('app_icon');
        $this->saveImage('app_splash_screen');

        return true;
    }

    private function saveImage(String $key)
    {
        // TODO: Mudar isso para array marge do antigo com o novo.
        if (!array_key_exists($key, $this->args['config'])) {
            $latestBuild = $this->app->latestBuild();

            $this->args['config'][$key] = $latestBuild->config[$key];
            return;
        }

        if (is_a($this->args['config'][$key], 'Illuminate\Http\UploadedFile')) {
            $image = $this->args['config'][$key];
            $label = $image->storePublicly('apps');
            $this->args['config'][$key] = $label;
        }
    }
}
