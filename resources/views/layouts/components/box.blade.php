<div class="row">
  <div class="col-md-12 p-3">
    <div class="card">
      <div class="card-header">{{ $title }}</div>
      <div class="card-body">
        {{ $slot }}
      </div>
    </div>
  </div>
</div>