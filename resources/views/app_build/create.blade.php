@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Create New Build for <b>{{ $build->app->app_name}}</b></div>
                <div class="card-body">
                  @include('layouts.partials.errors')

                  <form method="post" action="{{ route('apps.builds.store',['app' => $build->app]) }}" enctype="multipart/form-data" >
                      @csrf
                      @include('app_build.partials.form')
                  </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
