<?php

namespace App\Services\Shell;

use App\Services\Shell\BaseShell;

class GitShellService extends BaseShell
{
    public function newBranch($branchName)
    {
        return $this->shell([
            "git",
            "checkout",
            "-b",
            $branchName,
        ]);
    }

    public function commit($message = 'Auto generated')
    {
        return $this->shell([
            'git',
            'commit',
            '-m',
            "\"{$message}\"",
        ]);
    }

    public function addFiles()
    {
        return $this->shell([
            'git',
            'add',
            '.',
        ]);
    }

    public function checkout($branchName)
    {
        return $this->shell([
            'git',
            'checkout',
            $branchName,
        ]);
    }

    public function resetOrigin($branchName)
    {
        return $this->shell([
            'git',
            'reset',
            '--hard',
            "origin/{$branchName}",
        ]);
    }

    public function pull()
    {
        return $this->shell([
            'git',
            'pull',
        ]);
    }

    public function push()
    {
        return $this->shell([
            'git',
            'push',
        ]);
    }
}
