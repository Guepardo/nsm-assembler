<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class App extends Model
{
    protected $fillable = [
        'app_name',
        'app_display_name',
    ];

    public function builds() {
        return $this->hasMany(\App\Build::class);
    }

    public function unProcessedBuilds() {
        return $this->builds()->where('processed', false);
    }

    public function latestBuild() {
        return $this->builds()->latest()->first();
    }
}
